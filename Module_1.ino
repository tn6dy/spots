 #include <SoftwareSerial.h> 
 #include <TinyGPS++.h> 
 #include <Firebase.h> 
 #include <FirebaseArduino.h> 
 #include <FirebaseCloudMessaging.h> 
 #include <FirebaseError.h> 
 #include <FirebaseHttpClient.h> 
 #include <FirebaseObject.h> 
 #include <ESP8266WiFi.h> 
 float _lat = 0,_lon = 0; 
 float _receivedLat = 0, _receivedLon = 0; 
 static const int RxPin = 4; 
 static const int TxPin = 5; 
 static const int GSMRxPin = 2; 
 static const int GSMTxPin = 0; 
 int _distance = 0; 
 SoftwareSerial ss(RxPin,TxPin); 
 SoftwareSerial SIM800L(GSMRxPin,GSMTxPin); 
 TinyGPSPlus gps; 
 int flag = 0; 
 #define FIREBASE_HOST "tracking-system-2bab6-default-rtdb.firebaseio.com" 
 #define FIREBASE_AUTH "GYyio3vsCUy47egLOLIrFuIGVj912QCo3sPgdLZ0" 
 #define WIFI_SSID "OnePlus7Pro" 
 #define WIFI_PASSWORD "tarunhotspot"

void setup() {
  // put your setup code here, to run once:
 Serial.begin(115200); 
 WiFi.mode(WIFI_STA); 
 ss.begin(9600); 
 SIM800L.begin(19200); 
 WiFi.begin(WIFI_SSID,WIFI_PASSWORD); 
 Serial.print("connecting"); 
 while(WiFi.status() != WL_CONNECTED){ 
 Serial.print("."); 
 delay(500); 
 } 
 Serial.println(); 
 Serial.println("connected"); 
 Serial.print(WiFi.localIP()); 
 Firebase.begin(FIREBASE_HOST,FIREBASE_AUTH); 
}

void loop() {
  // put your main code here, to run repeatedly:
// This sketch displays information every time a new sentence is correctly encoded. 
 while (ss.available() > 0) 
 if (gps.encode(ss.read())){ 
 displayInfo(); 
 yield(); 
 yield(); 
 yield(); 
 if(Firebase.failed()){ 
 Serial.println(Firebase.error()); 
 ESP.reset(); 
 } 
 receive_Data_From_FB();
yield(); 
 yield(); 
 yield(); 
 _distance = gps.distanceBetween(_lat,_lon,_receivedLat,_receivedLon); 
 delay(300); 
 Firebase.setInt("distance",_distance); 
 yield(); 
 yield(); 
 yield(); 
 if(_distance>10){ 
 sendSMS(); 
 delay(1000); 
 } 
 Serial.print("distance is "); 
 Serial.println(_distance); 
 } 
 if (millis() > 5000 && gps.charsProcessed() < 10) { 
 Serial.println(F("No GPS detected: check wiring.")); 
 while(true); 
 } 
}
void displayInfo() { 
 Serial.print(F("Location: ")); 
 if (gps.location.isValid()) { 
 _lat = gps.location.lat(); 
 Serial.print(_lat, 6); 
 Serial.print(F(",")); 
 _lon = gps.location.lng(); 
 Serial.print(_lon, 6); 
 //delay(10000); 
 //Serial.print(gps.satellites.value()); 
//send_data_to_FB(); 
 } else { 
 Serial.print(F("INVALID")); 
 //delay(10000); 
 //send_data_to_FB(); 
 } 
 Serial.println(); 
 } 
 void sendSMS(){ 
 //Set Exact Baud rate of the GSM/GPRS Module. 
// Serial.begin(9600); 
 SIM800L.print("\r"); 
 delay(1000); 
 SIM800L.print("AT+CMGF=1\r"); 
 delay(1000); 
 SIM800L.print("AT+CMGW=\"+15737304144\"\r");
 delay(1000); 
 //The text of the message to be sent. 
 SIM800L.println("user location is varied please open the application"); 
 delay(1000); 
 SIM800L.print("\r\n");
 delay(1000);
 SIM800L.print("AT+CMSS=1\r\n");
 delay(1000);
 SIM800L.write(0x1A); 
 delay(1000); 
 } 
 void receive_Data_From_FB(){ 
 _receivedLat = Firebase.getFloat("latitude"); 
 _receivedLon = Firebase.getFloat("longitude"); 
 yield(); 
 yield(); 
 yield(); 
 Firebase.setInt("distance",_distance); 
 yield(); 
 yield();
delay(500); 
 } 
