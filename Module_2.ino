#include <Firebase.h> 
#include <FirebaseArduino.h> 
#include <FirebaseCloudMessaging.h> 
#include <FirebaseError.h> 
#include <FirebaseHttpClient.h> 
#include <FirebaseObject.h> 
#include <TinyGPS++.h> 
#include <SoftwareSerial.h> 
#include <ESP8266WiFi.h> 
static  const  int  RXPin  =  4,  TXPin  =  5;
static  const  uint32_t  GPSBaud  =  9600;
float  _lat=0,_lon  =  0;
//  The  TinyGPS++  object
TinyGPSPlus  gps;
#define  FIREBASE_HOST  "tracking-system-2bab6-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "GYyio3vsCUy47egLOLIrFuIGVj912QCo3sPgdLZ0" 
#define WIFI_SSID "Pixel" 
#define WIFI_PASSWORD "qwertyuiop" 
 // The serial connection to the GPS device 
SoftwareSerial ss(RXPin, TXPin); 
int n = 0;
void setup() {
  // put your setup code here, to run once:
WiFi.mode(WIFI_STA); 
 Serial.begin(115200); 
 ss.begin(9600); 
 Serial.println(gps.satellites.value());


WiFi.begin(WIFI_SSID, WIFI_PASSWORD); 
Serial.print("connecting"); 
while (WiFi.status() != WL_CONNECTED)
{ 
Serial.print("."); 
 delay(500); 
 } 
Serial.println(); 
Serial.print("connected: "); 
Serial.println(WiFi.localIP()); 
Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH); 
 } 

void loop() {
  // put your main code here, to run repeatedly:
// This sketch displays information every time a new sentence is correctly encoded. 
while (ss.available() > 0) 
if (gps.encode(ss.read())){ 
displayInfo(); 
yield(); 
yield(); 
yield(); 
if(Firebase.failed()){ 
Serial.println(Firebase.error()); 
//Serial.println("hi"); 
//ESP.reset(); 
 } 
 send_data_to_FB(); 
 } 
 if (millis() > 5000 && gps.charsProcessed() < 10) { 
 Serial.println(F("No GPS detected: check wiring.")); 
 while(true); 
 } 
 }

void displayInfo() { 
Serial.print(F("Location: ")); 
if (gps.location.isValid()) { 
 _lat = gps.location.lat(); 
 Serial.print(_lat, 6); 
 Serial.print(F(",")); 
 _lon = gps.location.lng(); 
 Serial.print(_lon, 6); 
 //delay(10000); 
 //Serial.print(gps.satellites.value()); 
 //send_data_to_FB(); 
 } else { 
 Serial.print(F("INVALID")); 
 //delay(10000); 
 //send_data_to_FB(); 
 } 

Serial.println(); 
 } 
 void send_data_to_FB(){ 
Firebase.setFloat("latitude",_lat); 
 delay(500); 
 Firebase.setFloat("longitude",_lon); 
 delay(500); 
 Firebase.setInt("Test_val",++n); 
 delay(500); 
 yield(); 
 yield(); 
 yield(); 
//Serial.println(n); 
 } 
